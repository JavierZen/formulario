function limpia() {
    var app = document.querySelector('#resultado');
    app.innerHTML = " ";
}

$(function() {
    $('.soloTexto').keyup(function() {
        var regex = /[^a-z]/gi;
        this.value = this.value.replace(regex, "");

    });
});



function validateRegEx(regex, input, helpText, helpMessage) {
    // See if the input data validates OK
    if (!regex.test(input)) {
        // The data is invalid, so set the help message and return false
        if (helpText != null)
            helpText.innerHTML = helpMessage;
        return false;
    } else {
        // The data is OK, so clear the help message and return true
        if (helpText != null)
            helpText.innerHTML = "";
        return true;
    }
}


function validateNombre(inputField, helpText) {

    inputField.value = inputField.value.toLocaleUpperCase();

    validateRegEx(/^[a-zA-Z ]+$/,
        inputField.value, helpText,
        "Please enter your name");
}


function validateApellidos(inputField, helpText) {

    inputField.value = inputField.value.toLocaleUpperCase();

    validateRegEx(/^[a-zA-Z ]+$/,
        inputField.value, helpText,
        "Please enter your second name");
}


function validateNif(inputField, helpText) {

    inputField.value = inputField.value.toLocaleUpperCase();

    validateRegEx(/^\d{4,8}-[a-zA-Z]{1}$/,
        inputField.value, helpText,
        "Please enter a valid NIF.");
}


function validateEmail(inputField, helpText) {

   // inputField.value = inputField.value.toLocaleUpperCase();

    validateRegEx(/\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b/,
        inputField.value, helpText,
        "Please enter an email address (for example, none@dominio.com).");
}


function validateEdad(inputField, helpText) {

    edad = inputField.value;

    if ((edad < 1) || (edad > 105)) {
        helpText.innerHTML = "Introduzca un número => 0 y <= 105";
        return false;
    } else {
        helpText.innerHTML = "";
        return true;
    }
}




function checkForm() {
    var nombre = document.forms["formulario"]["nombre"];
    var nombreHelp = document.getElementById("nombre_help").innerHTML;

    var apellidos = document.forms["formulario"]["apellidos"];
    var apellidosHelp = document.getElementById("apellidos_help").innerHTML;

    var edad = document.forms["formulario"]["edad"];
    var edadHelp = document.getElementById("edad_help").innerHTML;

    var nif = document.forms["formulario"]["nif"];
    var nifHelp = document.getElementById("nif_help").innerHTML;

    var email = document.forms["formulario"]["email"];
    var emailHelp = document.getElementById("email_help").innerHTML;

    var provincia = document.forms["formulario"]["provincia"];



    if (nombre.value == "" || nombreHelp != "") {
        window.alert("Please enter your Nombre.");
        nombre.focus();
        return false;
    }

    if (apellidos.value == "" || apellidosHelp != "") {
        window.alert("Please enter your Apellidos.");
        apellidos.focus();
        return false;
    }

    if (edad.value == "" || edadHelp != "") {
        window.alert(
            "Please enter your Age.");
        edad.focus();
        return false;
    }

    if (nif.value == "" || nifHelp != "") {
        window.alert(
            "Please enter your NIF.");
        nif.focus();
        return false;
    }

    if (email.value == "" || emailHelp != "") {
        window.alert("Please enter your Email");
        email.focus();
        return false;
    }

    if (provincia.value == "") {
        window.alert("Please enter your Provincia");
        provincia.focus();
        return false;
    }


    alert("todo OK ahora enviamos el codigo");


    items = Array();

    items[0] = nombre.value;
    items[1] = apellidos.value;
    items[2] = edad.value;
    items[3] = nif.value;
    items[4] = email.value;
    items[5] = provincia.value;


    var list = document.createElement('ul');

    items.forEach(function(item) {
        var li = document.createElement('li');
        li.textContent = item;
        list.appendChild(li);
    });

    var app = document.querySelector('#resultado');
    app.appendChild(list);

    return true;

}